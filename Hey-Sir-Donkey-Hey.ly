\version "2.18.2"

\language "english"
\include "ajb-setup.ly"
theStaffSize = #18
\include "ajb-paper.ly"
\include "ajb-layout.ly"
\include "articulate.ly"

\header {
  % dedication = "For the St George Singers"
  title = "Hey, Sir Donkey, Hey!"
  subtitle = "Orientis Partibus – The Song of the Ass"
  composer = "Traditional arr. & tr. Alex Ball"
  % Remove default LilyPond tagline
  copyright = \markup { \override #'(baseline-skip . 2.5) \small { \center-column {
    "© Alex Ball 2017. This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International Licence:"
    "https://creativecommons.org/licenses/by-nc/4.0/"
  } } }
  tagline = "Version 1.1 – 2018-03-26"
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \mixolydian
  \numericTimeSignature
  \time 6/8
  \tempo "Steadily" 4.=48
}

verseOne = \lyricmode {
  \set stanza = "1."
  Beau -- ti -- ful and no -- ble beast,
  bear -- ing Ma -- ry cross the East;
  strong and sui -- ted to the task,
  tra -- vel safe -- ly, tra -- vel fast.
  Hey, Sir Don -- key, hey!
}

verseTwo = \lyricmode {
  \set stanza = "2."
  Leave your home and tra -- vel far
  past the mount -- ains of Sy -- char,
  pass on by Je -- ru -- sa -- lem,
  come at last to Beth -- le -- hem.
  Hey, Sir Don -- key, hey!
}

verseThree = \lyricmode {
  \set stanza = "3."
  So much fast -- er he pro -- ceeds
  than a ca -- mel of the Medes.
  Ne -- ver could a kid or doe
  leap more nim -- b -- ly as they go.
  Hey, Sir Don -- key, hey!
}

verseFour = \lyricmode {
  \set stanza = "4."
  As the la -- den cart he draws,
  grind -- ing grain with pow’r -- ful jaws,
  he is gen -- tle, he is strong,
  faith -- ful all the jour -- ney long.
  Hey, Sir Don -- key, hey!
}

verseFive = \lyricmode {
  \set stanza = "5."
  When you reach that ho -- ly town,
  when you lay your bur -- den down,
  bar -- ley, this -- tle, straw or wheat,
  what will be your choice to eat?
  Hay, Sir Don -- key, hay?
}

verseSix = \lyricmode {
  \set stanza = "6."
  Say a -- men, O no -- ble ass,
  sa -- ti -- a -- ted by the grass.
  You who let no e -- vil nigh,
  raise your a -- men to the sky.
  Hey, Sir Don -- key, hey!
}

sopranoVoice = \relative c'' {
  \global
  \dynamicUp
  % Introduction
  R2. * 2
  % Verse 1
  g4\mp a8 b4 g8
  a8 f4 g4.
  d'4 d8 e4 b8
  d4 c8 b4.
  b4 a8 c4 b8
  a4 g8 b4.
  d4 c8 b8( a) g8
  a8 f4 g4.
  g4 a8 b4 a8
  g2.
  % Bridge
  R2. * 2
  % Verse 2
  R2. * 10
  % Bridge
  R2. * 2
  % Verse 3
  g4\mf a8 b4 g8
  a4 f8 g4.
  d'4 d8 e4 b8
  d4 c8 b4.
  b4 a8 c4 b8
  a4 g8 b4.
  d4 c8 b8. a16 g8
  a8 f4 g4.
  g4 a8 b4 a8
  g2.
  % Bridge
  R2. * 2
  % Verse 4
  b4\f c8 d4 b8
  c4 a8 b4.
  g'4 f8 e4 d8
  f4 e8 d4.
  d4 c8 e4 d8
  c4 b8 d4.
  g4 e8 d8( c) b8
  c8 a4 b4.
  b4 c8 d4 c8
  b2.
  % Bridge
  R2. * 2
  % Verse 5
  g4\mp a8 b4 g8
  a4 f8 g4.
  d'4 d8 e4 b8
  d4 c8 b4.
  b4 a8 c4 b8
  a4 g8 b4.
  d4 c8 b8( a) g8
  a4 f8 g4.
  g4 a8 b4 a8
  g2.
  % Bridge
  R2. * 2
  % Verse 6
  g4\f a8 b4 g8
  a4 f8 g4.
  d'4 d8 e4 b8
  d4 c8 b4.
  b4 a8 c4 b8
  a8 g4 b4.
  d4 c8 b8( a) g8
  a4 f8 g4.
  g4 a8 b4 c8
  d4.( b4.)
  g4 a8 b4 c8
  d4. ~ d4 r8
  % Coda
  f2.\ff
  e2.
  <d g>2. ~
  q4. ~ q8 r4
  \bar "|."
}

verseSopranoVoice = \lyricmode {
  \verseOne
  \verseThree
  \verseFour
  \verseFive
  \verseSix
  Hey, Sir Don -- key, hey!
  Say A -- men!
}

altoVoice = \relative c' {
  \global
  \dynamicUp
  % Introduction
  R2. * 2
  % Verse 1
  R2. * 10
  % Bridge
  R2. * 2
  % Verse 2
  R2. * 10
  % Bridge
  R2. * 2
  % Verse 3
  d4\mf e8 f4 g8
  f8( e) c d4.
  b'4 b8 c4 g8
  b4 a8 g4.
  b4 a8 g4 g8
  d4 e8 f4.
  g4 g8 g8. e16 d8
  f16( e) c4 d4.
  d4 e8 f4 e8
  d2.
  % Bridge
  R2. * 2
  % Verse 4
  g4\f a8 b4 g8
  a4 f8 g4.
  d'4 d8 e4 b8
  d4 c8 b4.
  b4 a8 c4 b8
  a4 g8 b4.
  d4 c8 b8( a) g8
  a8 f4 g4.
  g4 a8 b4 a8
  g2.
  % Bridge
  R2. * 2
  % Verse 5
  d4\mp f16( e) d4 e8
  e8 d4 d4.
  g4 g8 g4 d8
  g8 f4 d4.
  g4 d8 g4 g8
  e4 e8 d4.
  g4 e8 d4 e8
  e8 d4 d4.
  e4 e8 g4 f8
  d2.
  % Bridge
  R2. * 2
  % Verse 6
  R2. * 2
  g4\f a8 b4 g8
  a4 f8 g4.
  d'4 d8 e4 b8
  d4 c8 b4.
  b4 a8 c4 b8
  a8 g4 b4.
  d4 c8 b8( a) g8
  a4 f8 g4.
  g4 a8 b4 c8
  b4. ~ b4 r8
  % Coda
  c2.\ff
  a2.
  b2. ~
  b4. ~ b8 r4
}

verseAltoVoice = \lyricmode {
  \verseThree
  \verseFour
  \verseFive
  \verseSix
  Say A -- men!
}

tenorVoice = \relative c' {
  \global
  \dynamicUp
  % Introduction
  R2. * 2
  % Verse 1
  R2. * 10
  % Bridge
  R2. * 2
  % Verse 2
  g4\mf a8 b4 g8
  a8 f4 g4.
  d'4 d8 e4 b8
  d4 c8 b4.
  b4 a8 c4 b8
  a4 g8 b4.
  d4 c8 b8( a) g
  a4 f8 g4.
  g4 a8 b4 a8
  g2.
  % Bridge
  R2. * 2
  % Verse 3
  R2. * 10
  % Bridge
  R2. * 2
  % Verse 4
  d'4.\f b4 r8
  c4. b8 g b
  d4. b4 r8
  c4. b8 g b
  d4. c4 r8
  e4. f16( e) d8 c
  d4. b4 r8
  c4. b8 g b
  d4 e8 f4 e8
  d2.
  % Bridge
  R2. * 2
  % Verse 5
  b4\mp c8 g4 b8
  a4 a8 b4.
  b4 b8 b4 b8
  a4 a8 g4.
  d'4 d8 c4 d8
  e4 b8 b4.
  b4 a8 d4 b8
  c4 a8 b4.
  b4 a8 d4 c8
  b2.
  % Bridge
  R2. * 2
  % Verse 6
  g4\f a8 b4 g8
  a4 f8 g4.
  d'4 d8 e4 b8
  d4 c8 b4.
  b4 a8 c4 b8
  a8 g4 b4.
  d4 c8 b8( a) g8
  a4 f8 g4.
  g4 a8 b4 c8
  d4.( b4.)
  g4 a8 b4 a8
  g4. ~ g4 r8
  % Coda
  a2.\ff
  c2.
  <d g>2. ~
  q4. ~ q8 r4
}

verseTenorVoice = \lyricmode {
  \verseTwo
  % Verse 4
  Hey, Sir!
  Hey, Sir Don -- key!
  Hey, Sir!
  Hey, Sir Don -- key!
  Hey, Sir!
  Hey, Sir Don -- key!
  Hey, Sir!
  Hey, Sir Don -- key!
  Hey, Sir Don -- key, hey!
  \verseFive
  \verseSix
  Hey, Sir Don -- key, hey!
  Say A -- men!
}

bassVoice = \relative c' {
  \global
  \dynamicUp
  % Introduction
  R2. * 2
  % Verse 1
  R2. * 10
  % Bridge
  R2. * 2
  % Verse 2
  g4.\mf d8 r4
  d4. d8 d8 f8
  g4. g4.
  f4 e8 d4.
  g4. d8 r4
  d4. d8 d8 f8
  g4. g4.
  f4 e8 d4.
  d4 e8 f4 e8
  d2.
  % Bridge
  R2. * 2
  % Verse 3
  R2. * 10
  % Bridge
  R2. * 2
  % Verse 4
  g4.\f g4 r8
  g4. g8 d f
  g4. g4 r8
  g4. g8 d f
  g4. g4 r8
  g4. g8 d f
  g4. g4 r8
  g4. g8 d f
  g4 g8 g4 g8
  g2.
  % Bridge
  R2. * 2
  % Verse 5
  g4\mp f8 d4 e8
  c8 d4 g4.
  g4 g8 e4 g8
  d4 d8 g4.
  g4 f8 e4 d8
  c4 e8 b4.
  g4 a8 b4 e8
  c8 d4 g,4.
  e'4 c8 b4 c8
  g2.
  % Bridge
  R2. * 2
  % Verse 6
  R2. * 2
  g'4\f a8 b4 g8
  a4 f8 g4.
  d'4 d8 e4 b8
  d4 c8 b4.
  b4 a8 c4 b8
  a8 g4 b4.
  d4 c8 b8( a) g8
  a4 f8 g4.
  g4 a8 b4 a8
  g4. ~ g4 r8
  % Coda
  d2.\ff
  d2.
  <g, g'>2. ~
  q4. ~ q8 r4
}

verseBassVoice = \lyricmode {
  % Verse 2
  Hey, Sir!
  Hey, Sir Don -- key!
  Hey, Sir Don -- key, hey!
  Hey, Sir!
  Hey, Sir Don -- key!
  Hey, Sir Don -- key, hey!
  Hey, Sir Don -- key, hey!
  % Verse 4
  Hey, Sir!
  Hey, Sir Don -- key!
  Hey, Sir!
  Hey, Sir Don -- key!
  Hey, Sir!
  Hey, Sir Don -- key!
  Hey, Sir!
  Hey, Sir Don -- key!
  Hey, Sir Don -- key, hey!
  \verseFive
  \verseSix
  Say A -- men!
}

right = \relative c'' {
  \global
  % Introduction
  g2.
  g2.
  % Verse 1
  \repeat unfold 4 {
    g2.
    g2.
  }
  <<
    {
      g2. ~
     g2.
    } \\ {
      d4 e8 f4 e8
      d2.
    }
  >>
  % Bridge
  <g d>4. q
  q q
  % Verse 2
  \repeat unfold 4 {
    q q
    q q
  }
  <g d>4 <a e>8 <b g f>4 <a e>8
  <g d>4. q
  % Bridge
  f'8 e d e d c
  d8 c b c <b b,> <a c,>
  % Verse 3
  <g d b>4. <g b,>
  <g c,>4. <g d>4.
  <g b,>4. q
  <g c,>4. <g d>4.
  <g b,>4. q
  <g d c>4. <g d>4.
  <g b,>4. q
  <g c,>4. <g d>4.
  <g d b>4 <a e c>8 <b g f>4 <a e c>8
  <g d b>4. <g d b>4.
  % Bridge
  <g b>8 <a c> <b d> <a c> <b d> <c e>
  <b d>8 <c e> <d f> <c e> <b d> <a c>
  % Verse 4
  <b g>4 <c a>8 <d b>4 <b g>8
  <c a>4 <a f>8 <b g>4.
  <g' d>4 <f d>8 <e b>4 <d b>8
  <f d>4 <e c>8 <d b>4.
  <d b>4 <c a>8 <e c>4 <d b>8
  <c a e>4 <b g e>8 << { <d b>4. } \\ { f,16 e d8 r } >>
  <g' d>4 <e c>8 <d b> <c a> <b g>
  <c a>8 <a f>4 <b g>4.
  <b g d>4 <c a e>8 <d b g f>4 <c a e>8
  <b g d>4. <b g d>4.
  % Bridge
  <f a g c>4 r8 q4 r8
  q4 r8 q4 r8
  % Verse 5
  <g b d>8\staccato r4 r4.
  r4. r8 <b d g>\( <a c f>
  <g b d>8\) r4 r4.
  r4. r8 <b d g>\( <a c f>
  <g b d>8\) r4 r4.
  r4. r8 <d g b>\( <f a c>
  <g b d>8\) r4 r4.
  R2.
  <<
    {
      g4 a8 b4 a8
      g4. g4.
    } \\ {
      e4 e8 g4 f8
      d4. d4.
    }
  >>
  % Bridge
  <a' c f>4. <g c e>4.
  <g c d>4. <g a c>4.
  % Verse 6
  <d g b>4 <d a'>8 <d g b>4 <e g b>8
  <c e a>4 <c f>8 <b d g>4.
  <g' b d>4 <a d>8 <g b e>4 <g b>8
  <f a d>4 <f a c>8 <d g b>4.
  <g b d>4 <a d>8 <g b e>4 <g b>8
  << { <f d'>4 <e c'>8 } \\ { a8 g4 } >> <d g b>4.
  <g b d>4 <e a c>8
  << { <c' f,>4 b8 } \\ { b8( a) <g d>8 } >>
  <f g a c>4 q8 <d g b>4.
  <g b d>4 <a c>8 <e g b> <f a> <e g c>
  << { d'8 f e } \\ { <f, a>4 q8 } >> <g b d>4. 
  g4 a8 b4 <a c>8
  <g b d>4. q4.
  <a c f>4. q4.
  << { e'4 f8 ~ f g4 } \\ { <a, c>2. }  >>
  <g b d g>2. ~ 
  q4. ~ q8 r4
}

left = \relative c {
  \global
  % Introduction
  g4.\tenuto g8\staccato r4
  g4.\tenuto g8\staccato d f
  % Verse 1 + Bridge
  \repeat unfold 5 {
    g4.\tenuto g8\staccato r4
    g4.\tenuto g8\staccato d f
  }
  % Bridge + Verse 2
  \repeat unfold 6 {
    g4.\tenuto g8\staccato r4
    g4.\tenuto g8\staccato d f
  }
  % Bridge + Verse 3
  \repeat unfold 6 {
  <<
    {
      g'4._\tenuto g8_\staccato r4
      g4._\tenuto g8_\staccato d f  
    } \\ {
      g,4. g4.
      g4. g4.
    }
  >>
  }
  % Bridge + Verse 4
  <<
    {
      g'4._\tenuto g8_\staccato r4
      g4._\tenuto g8_\staccato d f
      \repeat unfold 2 {
        <g d'>4._\tenuto <g b>8_\staccato r4
        <g c>4._\tenuto <g b>8_\staccato <d g> <f b>
      }
      <g d'>4._\tenuto <g c>8_\staccato r4
      <g c>4._\tenuto <g b>8_\staccato <d g> <f c'>  
      <g d'>4._\tenuto <g b>8_\staccato r4
      <g c>4._\tenuto <g b>8_\staccato <d g> <f b>
      g4._\tenuto g8_\staccato r4
      g4._\tenuto g8_\staccato d f
    } \\ {
      \repeat unfold 6 {
        g,4. g4.
        g4. g4.
      }
    }
  >>
  % Bridge
  <<
    {
      f'8 d f f d f
      f d f c' b a
    } \\ {
      f,4. f4.
      a4. f4.
    }
  >>
  % Verse 5
  <g g'>8\staccato r4 r4.
  r4. r8 <d d'>\( <f f'>
  <g g'>8\) r4 r4.
  r4. r8 <d d'>\( <f f'>
  <g g'>8\) r4 r4.
  r4. r8 <b b'>\( <a a'>
  <g g'>8\) r4 r4.
  <<
    {
      R2.
      b'4 a8 d4 c8
      b4. b4.
    } \\ {
      r4. r8 g f
      e4 c8 b4 c8
      g4. g4.
    }
  >>
  % Bridge
  <<
    {
      g'8 g4 ~ g8 g4 ~
      g8 g4 ~ g8 g4
    } \\ {
      c,4 c8 c4 c8
      c4 c8 c4 c8
    }
  >>
  % Verse 6
  <<
    {
      g'8 g4 g8 g4
      g8 g4 g8 d f
      \repeat unfold 2 {
        g8 g4 g8 g4
        g8 g4 g8 d f
      }
    } \\ {
      g,4. g4.
      g4. g8 d f
      g4. g4.
      g4. g8 d f
      g4. g4.
      g4. g8 d f
    }
  >>
  <g g'>4 <a a'>8 <b b'> <a a'> <b b'>
  <c c'>8 <d d'>4 <g, g'>8 <d d'> <f f'>
  <g g'>4 <a a'>8 <b b'>4 <g g'>8
  <a a'>4 <f f'>8 <g g'>8 <d d'> <f f'>
  <g g'>4 <a a'>8 <b b'>4 <a a'>8
  <<
    {
      g'8 g g g g g
      d8 f d d f d
      d8 g d d g d
    } \\ {
      g,4. g4.
      <g d>4. q
      q q
    }
  >>
  <g g'>4 <a a'>8 <b b'>4 <a a'>8
  <g g'>8. q16 q8 q r4
}

dynamics = {
  % Introduction
  s4.\mp s4.
  s2.
  % Verse 1
  s2. * 9
  s4. s8\< s4
  % Bridge
  s4.\mf s4.
  s2.
  % Verse 2
  s2. * 10
  % Bridge
  s2. * 2
  % Verse 3
  s2. * 10
  % Bridge
  s8\cresc s4 s4.
  s2.
  % Verse 4
  s4.\f s4.
  s2. * 9
  % Bridge
  s2. * 2
  % Verse 5
  s2.
  s4. s8 s8\p s8
  s2. * 5
  s4. s8 s8\mp s8
  s2. * 2
  % Bridge
  s8\mf\< s4 s4.
  s2.
  % Verse 6
  s8\f s4 s4.
  s2. * 10
  s8\< s4 s4.
  s8\ff s4 s4.
  s2. * 3
}

sopranoVoicePart = \new Staff \with {
  instrumentName = "S."
  shortInstrumentName = \markup { \tiny "S" }
  midiInstrument = "choir aahs" % "harpsichord" %
} { \sopranoVoice }
\addlyrics { \verseSopranoVoice }

altoVoicePart = \new Staff \with {
  instrumentName = "A."
  shortInstrumentName = \markup { \tiny "A" }
  midiInstrument = "choir aahs" % "harpsichord" %
} { \altoVoice }
\addlyrics { \verseAltoVoice }

tenorVoicePart = \new Staff \with {
  instrumentName = "T."
  shortInstrumentName = \markup { \tiny "T" }
  midiInstrument = "choir aahs" % "harpsichord" %
} { \clef "treble_8" \tenorVoice }
\addlyrics { \verseTenorVoice }

bassVoicePart = \new Staff \with {
  instrumentName = "B."
  shortInstrumentName = \markup { \tiny "B" }
  midiInstrument = "choir aahs" % "harpsichord" %
} { \clef bass \bassVoice }
\addlyrics { \verseBassVoice }

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ" % "acoustic grand" %
  } \right
  \new Dynamics { \dynamics }
  \new Staff = "left" \with {
    midiInstrument = "church organ" % "acoustic grand" %
  } { \clef bass \left }
>>

\score {
  <<
    \new ChoirStaff <<
      \sopranoVoicePart
      \altoVoicePart
      \tenorVoicePart
      \bassVoicePart
    >>
    \pianoPart
  >>
  \layout {
    \context {
      \Lyrics
      \override LyricSpace.minimum-distance = #0.8
    }
  }
}

\score { \articulate
  <<
    \new ChoirStaff <<
      \sopranoVoicePart
      \altoVoicePart
      \tenorVoicePart
      \bassVoicePart
    >>
    \pianoPart
  >>
  \midi { }
}
