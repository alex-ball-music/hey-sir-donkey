# Hey, Sir Donkey, Hey!

This is an arrangement of the mediaeval Song of the Ass, *Orientus Partibus*,
traditionally sung on the eve of the Feast of the Circumcision, that is, New
Year's Eve.

For more information on the original carol, see for example

  - E. de Coussemaker, ‘Orientis Partibus’, *Annales Archéologiques*, 16 (1856),
    259, 300–4.

  - Henry Copley Greene, ‘The Song of the Ass’, *Speculum*, 6/4 (1931), 534–549.
    doi: [10.2307/2849511](https://doi.org/10.2307/2849511)

  - Henri Villetard, *L’Office de Pierre de Corbeil*, (Bibliotheque
    Musicologique, 4; Paris: Picard, 1907), 86–7, 130-1,
    <https://archive.org/details/officedepierrede00pier>.

This arrangement evokes the ancient character of the carol but gives it a modern
twist. The words are my own metrical translation (somewhat loose) of the
original Latin into English.

## Summary information

  - *Voicing:* SATB (optional split in STB on last chord)

  - *Notes on ambitus:* ST go up to G; AB go up to E. There is scope for
    transposing down a tone if necessary.

  - *Instrumentation:* Organ

  - *Approximate performance length:* 3:15

  - *Licence:* Creative Commons Attribution-NonCommercial 4.0 International
     Public Licence: <https://creativecommons.org/licenses/by-nc/4.0/>

## Files

This repository contains the [Lilypond](http://lilypond.org) source code. To
compile it yourself, you will also need my [house style
files](https://gitlab.com/alex-ball-music/ajb-lilypond).

For PDF and MIDI downloads, see the [Releases
page](https://gitlab.com/alex-ball-music/hey-sir-donkey/-/releases).
